#!/usr/bin/bash
directory=$1
Description=`jq -r '.Description' $directory`
Package_path=`jq -r '.Package_path' $directory`
Service_url=`jq -r '.Service_url' $directory`
Service_path=`jq -r '.Service_path' $directory`

if [ -e /etc/systemd/system/netcore-api.service ]; then
        sudo systemctl daemon-reload netcore-api.service
else
    touch netcore-api.service
echo "[Unit]
Description=$Description

[Service]
ExecStart=$Package_path $Service_path --urls=$Service_url
Restart=on-failure
SyslogIdentifier=dotnet-first-service

[Install]
WantedBy=multi-user.target" > netcore-api.service

sudo cp netcore-api.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl start netcore-api.service
fi
